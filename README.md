vertigo-minus
-------------

a simple non-optimised [vertigo](http://conal.net/Vertigo/) variant

Conal Elliott "Programming Graphics Processors Functionally" 2004
([PDF](http://conal.net/papers/Vertigo/1206-elliott.pdf))

![](tn/vertigo-minus/png/e.png)

© [rohan drape](http://rohandrape.net/),
  2005-2024,
  [gpl](http://gnu.org/copyleft/)
