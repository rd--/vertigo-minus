import Graphics.Vertigo.Minus.Mesh {- vertigo-minus -}
import Graphics.Vertigo.Minus.Vertigo {- vertigo-minus -}

import qualified Sound.Sc3.Data.Geometry.Obj as Obj {- hsc3-data -}

r_to_double :: R -> Double
r_to_double = realToFrac

r3_to_v3 :: R3 -> (Double,Double,Double)
r3_to_v3 (R3 x y z) = (r_to_double x,r_to_double y,r_to_double z)

ln_to_obj :: Int -> FilePath -> [[R3]] -> IO ()
ln_to_obj k fn = Obj.obj_store_ln k fn . map (map r3_to_v3)

wr_e :: IO ()
wr_e = do
  let ln = mesh_surface_ln (mesh_grid_ix 50) (eggcrateS (2.6,0.23))
  ln_to_obj 4 "/tmp/e.obj" ln

{-
hmt-gl obj-gr /tmp/e.obj
-}
