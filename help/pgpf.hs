-- Conal Elliott "Programming Graphics Processors Functionally"
-- Haskell Workshop, 2004

import Graphics.Vertigo.Minus.Mesh
import Graphics.Vertigo.Minus.Vertigo
import Graphics.Vertigo.Minus.Viewer

mesh_surface_n :: R -> Surface -> IO ()
mesh_surface_n n f = mesh_surface_gl (mesh_grid_ix n) f

vwS :: Surface -> IO ()
vwS = viewer () (\_ _ -> return ()) . const . mesh_surface_n 50

{-
vwS (cylinder 1)
vwS (freqMag sphere (2,1))
-}

vwSs :: [Surface] -> IO ()
vwSs u = do
  let rot (x:xs) = xs ++ [x]
      rot [] = []
      md = mesh_surface_n 50 . head
      ed xs _ = return (rot xs)
  viewer u ed md

{-
vwSs [cylinder 1,cylinder' 1,cylinder'' 1]
vwSs [torusFrac 1.5 0.5 0.8 0.8,eggcrateS (2.6,0.23),rippleS (5.7,0.1)]
-}
