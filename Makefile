all:
	echo "vertigo-minus"

clean:
	rm -Rf dist dist-newstyle *~

push-all:
	r.gitlab-push.sh vertigo-minus

push-tags:
	r.gitlab-push.sh vertigo-minus --tags

indent:
	fourmolu -i Graphics

doctest:
	doctest -Wno-x-partial -Wno-incomplete-uni-patterns Graphics
