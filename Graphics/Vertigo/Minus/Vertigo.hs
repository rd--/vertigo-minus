module Graphics.Vertigo.Minus.Vertigo where

import Data.Complex {- base -}

import Graphics.GL {- OpenGLRaw -}

type R = GLfloat
data R2 = R2 !R !R deriving (Eq, Show)
data R3 = R3 !R !R !R deriving (Eq, Show)
type Surface = R2 -> R3

addR2 :: R2 -> R2 -> R2
addR2 (R2 i j) (R2 x y) = R2 (i + x) (j + y)

mulR2 :: R2 -> R2 -> R2
mulR2 (R2 a b) (R2 a' b') = R2 (a * a') (b * b')

negateR2 :: R2 -> R2
negateR2 (R2 a b) = R2 (negate a) (negate b)

fromIntegerR2 :: Integer -> R2
fromIntegerR2 n = let n' = fromInteger n in R2 n' n'

recipR2 :: R2 -> R2
recipR2 (R2 a b) = R2 (recip a) (recip b)

divR2 :: R2 -> R -> R2
divR2 (R2 x y) n = R2 (x / n) (y / n)

fromRationalR2 :: Rational -> R2
fromRationalR2 x = let x' = fromRational x in R2 x' x'

sqrtR2 :: R2 -> R2
sqrtR2 (R2 a b) = R2 (sqrt a) (sqrt b)

addR3 :: R3 -> R3 -> R3
addR3 (R3 i j k) (R3 x y z) = R3 (i + x) (j + y) (k + z)

mulR3 :: R3 -> R3 -> R3
mulR3 (R3 a b c) (R3 a' b' c') = R3 (a * a') (b * b') (c * c')

negateR3 :: R3 -> R3
negateR3 (R3 a b c) = R3 (negate a) (negate b) (negate c)

fromIntegerR3 :: Integer -> R3
fromIntegerR3 n = let n' = fromInteger n in R3 n' n' n'

recipR3 :: R3 -> R3
recipR3 (R3 a b c) = R3 (recip a) (recip b) (recip c)

fromRationalR3 :: Rational -> R3
fromRationalR3 x = let x' = fromRational x in R3 x' x' x'

sqrtR3 :: R3 -> R3
sqrtR3 (R3 a b c) = R3 (sqrt a) (sqrt b) (sqrt c)

sphere :: Surface
sphere (R2 u v) =
  let theta = 2 * pi * u
      phi = pi * v
  in R3 (cos theta * sin phi) (sin theta * sin phi) (cos phi)

cylinder :: R -> Surface
cylinder h (R2 u v) =
  let theta = 2 * pi * u
  in R3 (cos theta) (sin theta) (h * v)

type HeightField = R2 -> R

hfSurf :: HeightField -> Surface
hfSurf f (R2 u v) = R3 u v (f (R2 u v))

sinU :: R -> R
sinU = sin . (* (2 * pi))

cosU :: R -> R
cosU = cos . (* (2 * pi))

magnitudeR2 :: R2 -> R
magnitudeR2 (R2 u v) = magnitude (u :+ v)

ripple :: HeightField
ripple = sinU . magnitudeR2

scaleR2 :: R -> R2 -> R2
scaleR2 n (R2 i j) = R2 (i * n) (j * n)

scaleR3 :: R -> R3 -> R3
scaleR3 n (R3 i j k) = R3 (i * n) (j * n) (k * n)

freqMag :: Surface -> (R, R) -> Surface
freqMag f (freq, mag) = scaleR3 mag . f . scaleR2 freq

rippleS :: (R, R) -> Surface
rippleS = freqMag (hfSurf ripple)

cartF :: (a -> b -> c) -> (R -> a) -> (R -> b) -> R2 -> c
cartF op f g (R2 u v) = f u `op` g v

eggcrate :: HeightField
eggcrate = cartF (*) cosU sinU

eggcrateS :: (R, R) -> Surface
eggcrateS = freqMag (hfSurf eggcrate)

type Curve2 = R -> R2
type Curve3 = R -> R3

sweep :: Curve3 -> Curve3 -> Surface
sweep basis scurve (R2 u v) = basis u `addR3` scurve v

lift1 :: (a -> b) -> (p -> a) -> (p -> b)
lift1 h f1 x = h (f1 x)

lift2 :: (a -> b -> c) -> (p -> a) -> (p -> b) -> (p -> c)
lift2 h f1 f2 x = h (f1 x) (f2 x)

lift3 :: (a -> b -> c -> d) -> (p -> a) -> (p -> b) -> (p -> c) -> (p -> d)
lift3 h f1 f2 f3 x = h (f1 x) (f2 x) (f3 x)

addX :: (a -> R2) -> (a -> R3)
addX = lift1 (\(R2 y z) -> R3 0 y z)

addY :: (a -> R2) -> (a -> R3)
addY = lift1 (\(R2 x z) -> R3 x 0 z)

addZ :: (a -> R2) -> (a -> R3)
addZ = lift1 (\(R2 x y) -> R3 x y 0)

addYZ :: (a -> R) -> (a -> R3)
addYZ = lift1 (\x -> R3 x 0 0)

addXZ :: (a -> R) -> (a -> R3)
addXZ = lift1 (\y -> R3 0 y 0)

addXY :: (a -> R) -> (a -> R3)
addXY = lift1 (\z -> R3 0 0 z)

circle :: Curve2
circle = let f = lift2 R2 in cosU `f` sinU

cylinder' :: R -> Surface
cylinder' h = sweep (addZ circle) (addXY (* h))

type Transform1 = R -> R
type Transform2 = R2 -> R2
type Transform3 = R3 -> R3

onX :: Transform1 -> Transform3
onX f (R3 x y z) = R3 (f x) y z

onY :: Transform1 -> Transform3
onY f (R3 x y z) = R3 x (f y) z

onZ :: Transform1 -> Transform3
onZ f (R3 x y z) = R3 x y (f z)

onXY :: Transform2 -> Transform3
onXY f (R3 x y z) = let (R2 x' y') = f (R2 x y) in R3 x' y' z

onYZ :: Transform2 -> Transform3
onYZ f (R3 x y z) = let (R2 y' z') = f (R2 y z) in R3 x y' z'

onXZ :: Transform2 -> Transform3
onXZ f (R3 x y z) = let (R2 x' z') = f (R2 x z) in R3 x' y z'

rotate :: R -> Transform2
rotate theta (R2 x y) =
  let c = cos theta
      s = sin theta
  in R2 (x * c - y * s) (y * c + x * s)

rotY :: R -> Transform3
rotY theta = onXZ (rotate theta)

revolve :: Curve2 -> Surface
revolve curve (R2 u v) = rotY (2 * pi * u) (addZ curve v)

semiCircle :: Curve2
semiCircle = circle . (/ 2)

sphere' :: Surface
sphere' = revolve semiCircle

cylinder'' :: R -> Surface
cylinder'' h = onZ (* h) . revolve (\y -> R2 1 y)

torus :: R -> R -> Surface
torus sr cr =
  let f = lift2 addR2
      g = lift2 scaleR2
  in revolve (const (R2 sr 0) `f` (const cr `g` circle))

torusFrac :: R -> R -> R -> R -> Surface
torusFrac sr cr cfrac sfrac = torus sr cr . mulR2 (R2 cfrac sfrac)

normaliseR2 :: R2 -> R2
normaliseR2 x = x `divR2` (magnitudeR2 x)
