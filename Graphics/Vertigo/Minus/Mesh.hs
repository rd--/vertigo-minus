module Graphics.Vertigo.Minus.Mesh where

import Graphics.GL {- OpenGLRaw -}

import qualified Graphics.Vertigo.Minus.Vertigo as V

{- | Generate a mesh (as a sequence of lines) sampling R2 space at
  values in `g' where `f' is a surface function.
-}
mesh_surface_ln :: [V.R] -> V.Surface -> [[V.R3]]
mesh_surface_ln g f =
  let ln x =
        [ map (\y -> f (V.R2 x y)) g
        , map (\y -> f (V.R2 y x)) g
        ]
  in concatMap ln g

-- | Grey color, with opacity.
glColor_grey :: V.R -> V.R -> IO ()
glColor_grey f a = glColor4f f f f a

mesh_surface_gl :: [V.R] -> V.Surface -> IO ()
mesh_surface_gl g f = do
  glBlendFunc GL_SRC_ALPHA GL_ONE
  glEnable GL_BLEND
  glColor_grey 0.25 0.5
  let vtx (V.R3 x y z) = glVertex3f x y z
      ln dat = glBegin GL_LINE_STRIP >> mapM_ vtx dat >> glEnd
  mapM_ ln (mesh_surface_ln g f)

-- | Generate a /n/ place line over the -1/2,1/2 space.
mesh_grid_ix :: (Enum n, Fractional n) => n -> [n]
mesh_grid_ix n = let i = 1 / n in [-0.5, -0.5 + i .. 0.5]
