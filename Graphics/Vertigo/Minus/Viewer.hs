module Graphics.Vertigo.Minus.Viewer (viewer) where

import qualified Data.IORef as R {- base -}

import qualified Graphics.UI.GLUT as G {- GLUT -}

import Graphics.Vertigo.Minus.Vertigo (R3 (..))

glu_init :: IO ()
glu_init = do
  G.viewport G.$= (G.Position 0 0, G.Size 800 800)
  G.matrixMode G.$= G.Projection
  G.loadIdentity
  G.perspective 30 1 1 100
  G.matrixMode G.$= G.Modelview 0
  G.loadIdentity
  G.lookAt (G.Vertex3 0 0 10) (G.Vertex3 0 0 0) (G.Vector3 0 1 0)
  G.shadeModel G.$= G.Smooth
  G.clearColor G.$= G.Color4 0 0 0 0
  G.blend G.$= G.Enabled
  G.blendFunc G.$= (G.SrcAlpha, G.One)
  G.depthFunc G.$= Nothing
  G.lighting G.$= G.Disabled
  G.normalize G.$= G.Enabled
  G.actionOnWindowClose G.$= G.ContinueExecution

idle :: IO ()
idle = G.postRedisplay Nothing

visible :: G.Visibility -> IO ()
visible G.Visible = G.idleCallback G.$= Just idle
visible G.NotVisible = G.idleCallback G.$= Nothing

data State u = State R3 u
type MODEL u = u -> IO ()
type EDIT u = u -> Char -> IO u
type KEY_F = G.Key -> G.KeyState -> G.Modifiers -> G.Position -> IO ()

initState :: u -> IO (State u)
initState u = return (State (R3 20 30 0) u)

draw :: MODEL u -> State u -> IO (State u)
draw md (State (R3 x y z) u) = do
  G.clear [G.ColorBuffer]
  _ <- G.preservingMatrix $ do
    G.rotate x (G.Vector3 1 0 0)
    G.rotate y (G.Vector3 0 1 0)
    G.rotate z (G.Vector3 0 0 1)
    md u
  G.swapBuffers
  return (State (R3 x y z) u)

modifyIORefIO :: R.IORef a -> (a -> IO a) -> IO ()
modifyIORefIO s f = do
  c <- R.readIORef s
  c' <- f c
  R.writeIORef s c'

draw' :: R.IORef (State u) -> MODEL u -> IO ()
draw' s md = modifyIORefIO s (draw md)

modRot :: R3 -> State u -> State u
modRot (R3 dx dy dz) (State (R3 x y z) u) =
  let p = R3 (x + dx) (y + dy) (z + dz)
  in State p u

modRot' :: R.IORef (State u) -> R3 -> IO ()
modRot' s d = R.modifyIORef s (modRot d)

runEdit :: EDIT u -> Char -> State u -> IO (State u)
runEdit e x (State v u) = do
  u' <- e u x
  return (State v u')

{- | Keyboard control:

Rotate: X=Up/Down Y=Left/Right Z=Page-Up/Down
Exit: Del
-}
keyboard :: EDIT u -> R.IORef (State u) -> KEY_F
keyboard e s k ks _ _ =
  case (k, ks) of
    (G.SpecialKey G.KeyPageUp, _) -> modRot' s (R3 0 0 5)
    (G.SpecialKey G.KeyPageDown, _) -> modRot' s (R3 0 0 (-5))
    (G.SpecialKey G.KeyUp, _) -> modRot' s (R3 5 0 0)
    (G.SpecialKey G.KeyDown, _) -> modRot' s (R3 (-5) 0 0)
    (G.SpecialKey G.KeyLeft, _) -> modRot' s (R3 0 5 0)
    (G.SpecialKey G.KeyRight, _) -> modRot' s (R3 0 (-5) 0)
    (G.Char '\DEL', _) -> G.leaveMainLoop
    (G.Char x, G.Down) -> modifyIORefIO s (runEdit e x)
    _ -> return ()

viewer :: u -> EDIT u -> MODEL u -> IO ()
viewer u ed md = do
  _ <- G.initialize "Mesh" []
  G.initialDisplayMode G.$= [G.RGBAMode, G.DoubleBuffered]
  G.initialWindowSize G.$= G.Size 800 800
  G.initialWindowPosition G.$= G.Position 0 0
  _ <- G.createWindow "GL"
  glu_init
  s <- initState u >>= R.newIORef
  G.displayCallback G.$= draw' s md
  G.keyboardMouseCallback G.$= Just (keyboard ed s)
  G.visibilityCallback G.$= Just visible
  G.mainLoop
